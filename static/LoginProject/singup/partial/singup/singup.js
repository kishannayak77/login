angular.module('singup').controller('SingupCtrl', function($location,$http, $scope, singup, $state, $rootScope,$q) {
    $scope.submit1 = function(data) {
        console.log('kishannnnn', data)
        singup.submits(data).then(function(data) {
            $scope.signupData = data;
            $state.go("login")


        })

    }

    $scope.Login = function(data) {
        // console.log('kikiki', data)
        singup.logins(data).then(function(data) {
            console.log('details', data);
            if (data.status == 200) {
                $rootScope.details = data['details'][0];
                $rootScope.logoutlogo = false
                console.log('successful login', $rootScope.logoutlogo);
                $state.go("details")
            } else {
                alert('Invalid Credentials');
                console.log('failed to login');
            }
        })
    }

    // $scope.logout = function(username) {
    //     console.log('logout', username)
    //     singup.logoutss(username).then(function(data) {
    //         $scope.logouts = data;
    //     })
    // }


    $scope.logout =function(username){
    	console.log("username", username)
    	var deferred =$q.defer(),
    	url = "http://127.0.0.1:8000/logouts";

    	$http({
    		url: url,
    		method: "GET",
    		data : username
    	})
    	.success(angular.bind(this,function(data,status,header,config){
    		deferred.resolve(data,status);
    		$location.path('/login')

    	}))
    	.error(angular.bind(this,function(data,status,header,config){
    		deferred.reject(data,status);
    }));
    	return deferred.promise;
    }
});
