angular.module('singup', ['ui.bootstrap', 'ui.router', 'ngAnimate']);

angular.module('singup').config(function($stateProvider) {
    $stateProvider.state('singup', {
        url: '/',
        templateUrl: 'singup/partial/singup/singup.html',
        controller: 'SingupCtrl'
    });
    $stateProvider.state('login', {
        url: '/login',
        templateUrl: 'singup/partial/singup/login.html',
        controller: 'SingupCtrl'
    });
    $stateProvider.state('details', {
        url: '/details',
        templateUrl: 'singup/partial/singup/details.html',
        controller: 'SingupCtrl'
    });
});
