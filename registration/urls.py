from django.conf.urls import url
from .views import signup,loginuser,logouts,check

urlpatterns = [
    url(r'^signupd', signup),
    url(r'^loginuser', loginuser),
    url(r'^logouts',logouts),
   url(r'^check',check)

]
